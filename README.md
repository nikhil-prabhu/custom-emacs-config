# custom-emacs-config
My custom, simple configuration for GNU Emacs.

# Introduction

- I'm quite new to GNU Emacs, and I've just got started with learning Emacs Lisp. So this config file will probably be a little messy or redundant. Suggestions on improvement are welcome :)

- The main purpose of this repository is to help me keep my Emacs configuration in sync across all my devices (my personal and work devices). So there won't be too much eyecandy or fancy features; Just the features I need for my personal workflow (In other words, this is a configuration tailored to my workflow; It's not a super fancy/massive configuration that others may want to use).

- All configuration is contained within the .emacs file. There are no other files/folders that are needed.

# Packages Used

* [nasm-mode](https://github.com/skeeto/nasm-mode) (Major mode for editing NASM x86 assembly programs)
* [gh-md](https://github.com/emacs-pe/gh-md.el) (To render markdown using the Github API)
* [helm-themes](https://github.com/syohex/emacs-helm-themes) (Provides a helm interface for picking themes)
* [diff-hl](https://github.com/dgutov/diff-hl) (Highlights changed lines in files)
* [snazzy-theme](https://github.com/weijiangan/emacs-snazzy)
* [ac-helm](https://github.com/yasuyk/ac-helm) (Autocomplete)
* [browse-kill-ring](https://github.com/browse-kill-ring/browse-kill-ring) (Lets you easily access and browse the kill ring)
* [flycheck](https://github.com/flycheck/flycheck) (Syntax checking)
* [telephone-line](https://github.com/dbordak/telephone-line) (Fancy customizable modeline; alternative to Powerline)
* org (Org mode)
* [evil](https://github.com/emacs-evil/evil) (EVIL mode; Provides Vim keybindings within Emacs)

# Package Archives Used

- [ELPA](https://elpa.gnu.org) - The GNU Emacs Lisp Package Archive
- [MELPA](https://melpa.org) - Milkypostman's Emacs Lisp Package Archive

# Installation

Clone the repository with git

```git clone https://github.com/nikhil-prabhu/custom-emacs-config```

Now from the `custom-emacs-config` folder, copy the `.emacs` file and paste it in the appropriate location. On Windows, it should be

```C:\Documents and Settings\Users\<username>\AppData\Roaming\.emacs```

On Unix-like systems (macOS, GNU/Linux, etc.), it should be

```~/.emacs```

You can now start Emacs. To automatically install the required packages, first refresh the package archives.

```M-x package-refresh-contents```

Then, install all required packages.

```M-x package-install-selected-packages```

Restart Emacs, and the configuration should work.

# Custom Keybindings

* Launch ansi-term: ```C-x t```
* Launch shell: ```C-x c``` (I use shell on Windows because ansi-term doesn't work)
* Toggle line numbers: ```C-x n```
* Browse kill ring: ```C-x C-k```
* Reload buffer without confirmation: ```C-c r```
* Launch eshell in a new buffer: ```C-x e``` (I sometimes use eshell instead of shell on Windows. Really depends on my mood and use-case)
* Render markdown using Github API: ```C-x g```
* Use helm-for-files to search for files/buffers: ```C-x f```
* Compile and run assembly program using NASM: ```C-c a```
