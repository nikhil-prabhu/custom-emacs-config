;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Initial Configuration ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;

(package-initialize)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (snazzy)))
 '(custom-safe-themes t)
 '(frame-brackground-mode (quote dark))
 '(package-selected-packages
   (quote
    (nasm-mode rainbow-delimiters indent-guide gh-md helm-themes diff-hl snazzy-theme ac-helm browse-kill-ring flycheck telephone-line org-link-minor-mode evil))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; Always start Emacs maximized
(add-to-list 'default-frame-alist '(fullscreen . maximized))

;; Add MELPA to package archives
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

;; Add emacs-application-framework to load-path
(if (eq system-type 'linux)
    (add-to-list 'load-path "~/.emacs.d/emacs-application-framework"))

;; Disable signature verification
(setq package-check-signature nil)

;;;;;;;;;;;;;;;;;;;;;;
;; Custom Functions ;;
;;;;;;;;;;;;;;;;;;;;;;

;; Revert (reload) buffer without confirmation
(defun revert-buffer-no-confirm ()
  (interactive)
  (revert-buffer :ignore-auto :noconfirm))

;; Launch eshell in new buffer
(defun launch-eshell-buffer ()
  (interactive)
  (split-window-sensibly)
  (switch-to-buffer '"eshell")
  (eshell-mode))

;; Compile .asm file, link object file, and create executable file
(defun compile-assembly-file ()
  (interactive)
  (shell-command-to-string (concat "nasm -f elf " (buffer-file-name)))
  (shell-command-to-string (concat "ld -m elf_i386 -s -o "
				   (concat
				    (car (split-string (buffer-file-name) "\\."))
				    (concat
				     " "
				     (concat
				      (car (split-string (buffer-file-name) "\\."))
				      ".o")))))
  (split-window-sensibly)
  (message "Done")
  (ansi-term (car (split-string (buffer-file-name) "\\."))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Package Configuration ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; evil-mode
(require 'evil)
(evil-mode 1)

;; eaf
(if (eq system-type 'linux)
    (require 'eaf))

;; ac-helm
(require 'ac-helm)
(helm-mode 1)

;; telephone-line
(require 'telephone-line)
(telephone-line-mode 1)

;; hl-line-mode
(global-hl-line-mode 1)

;; global-visual-line-mode (word wrap)
(global-visual-line-mode 1)

;; diff-hl
(require 'diff-hl)
(global-diff-hl-mode 1)

;; browse-kill-ring
(require 'browse-kill-ring)
(browse-kill-ring-default-keybindings)

;; indent-guide
(require 'indent-guide)
(indent-guide-global-mode 1)

;; auto-complete
(ac-config-default)

;; windmove
(windmove-default-keybindings)

;; flycheck
(add-hook 'after-init-hook #'global-flycheck-mode)

;; rainbow-delimiters
(add-hook 'emacs-lisp-mode-hook #'rainbow-delimiters-mode-enable)

;;;;;;;;;;;;;;;;;;;;;;;;
;; Custom Keybindings ;;
;;;;;;;;;;;;;;;;;;;;;;;;

;; Launch ansi-term
(global-set-key (kbd "\C-x t") 'ansi-term)

;; Launch shell
(global-set-key (kbd "\C-x c") 'shell)

;; Toggle line numbers
(if (boundp 'global-display-line-numbers-mode)
    (global-set-key (kbd "\C-x n") 'display-line-numbers-mode)
  (global-set-key (kbd "\C-x n") 'linum-mode)) ;; For emacs version < 26.x

;; Browse kill ring
(global-set-key (kbd "\C-x \C-k") 'browse-kill-ring)

;; Revert (reload) buffer without confirmation
(global-set-key (kbd "\C-c r") 'revert-buffer-no-confirm)

;; Launch eshell mode in new buffer
(global-set-key (kbd "\C-x e") 'launch-eshell-buffer)

;; Render markdown using Github API
(global-set-key (kbd "\C-x g") 'gh-md-render-buffer)

;; Rebind C-x f to helm-for-files command
(global-set-key (kbd "\C-x f") 'helm-for-files)

;; Compile and run .asm file
(global-set-key (kbd "\C-c a") 'compile-assembly-file)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Miscellaneous Settings ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;; Windows specific settings ;;;;

;; Inhibit startup message (to make default-directory setting work)
(if (eq system-type 'windows-nt)
    (setq inhibit-startup-message t))

;; Set default directory to C:/Users/<username>/AppData/Roaming ("~/")
(if (eq system-type 'windows-nt)
    (setq default-directory "~/"))

;; Set default ghostscript program to gswin64c.exe
(if (eq system-type 'windows-nt)
    (setq doc-view-ghostscript-program "gswin64c.exe"))

;; Disable toolbar (due to theme inconsistencies)
(if (eq system-type 'windows-nt)
    (tool-bar-mode -1))

;;;; Linux specific settings ;;;;

;; Set default ghostscript program to gs
(if (eq system-type 'linux)
    (setq doc-view-ghostscript-program "gs"))

;;;; Other settings ;;;;

;; Disable backup files
(setq backup-inhibited t)

;; Disable scroll bar
(scroll-bar-mode -1)

;; Set default tab size as 4 spaces for C
(setq c-basic-offset 4)

;; Set nasm-mode as major mode for .asm files
(add-to-list 'auto-mode-alist '("\\.asm\\'" . nasm-mode))

;; Disable alarms (because they can be really annoying at times)
(setq ring-bell-function 'ignore)

;; Set insert as the initial evil-mode state for eaf-mode
(evil-set-initial-state 'eaf-mode 'insert)
